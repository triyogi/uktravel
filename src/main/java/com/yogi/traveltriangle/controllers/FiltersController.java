package com.yogi.traveltriangle.controllers;

import com.yogi.traveltriangle.DTO.FiltersDTO;
import com.yogi.traveltriangle.models.Filters;
import com.yogi.traveltriangle.services.FiltersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FiltersController {

    @Autowired
    private FiltersService filtersService;

    @RequestMapping(value = "/home/createfilters",method = RequestMethod.POST)
    public Filters createFilters(@RequestBody FiltersDTO filtersDTO){
        return filtersService.createFilters(filtersDTO);
    }

    @RequestMapping(value = "home/getallfilters",method = RequestMethod.GET)
    public List<Filters> getallfilters(@RequestParam("hotelStarRating") String hotelStarRating) {
        List<Filters> filters = filtersService.getAllFilters(hotelStarRating);
        return filters;
    }
}
