package com.yogi.traveltriangle.controllers;

import com.yogi.traveltriangle.DTO.ReviewsDTO;
import com.yogi.traveltriangle.models.Reviews;
import com.yogi.traveltriangle.services.ReviewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ReviewsController {

    @Autowired
    private ReviewsService reviewsService;

    @RequestMapping(value = "/home/createreviews",method = RequestMethod.POST)
    public Reviews createReviews(@RequestBody ReviewsDTO reviewsDTO){
        return reviewsService.createReviews(reviewsDTO);
    }

    @RequestMapping(value = "home/getallreviews",method = RequestMethod.GET)
    public List<Reviews> getAllReviews(@RequestParam(value = "reviewtitle") String reviewtitle){

        List<Reviews> myReviews = reviewsService.getAllReviews(reviewtitle);
    return myReviews;
    }
}
