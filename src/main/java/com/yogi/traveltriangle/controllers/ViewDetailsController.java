package com.yogi.traveltriangle.controllers;

import com.yogi.traveltriangle.DTO.ViewDetailsDTO;
import com.yogi.traveltriangle.models.Packages;
import com.yogi.traveltriangle.models.ViewDetails;
import com.yogi.traveltriangle.services.ViewDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ViewDetailsController {

    @Autowired
    private ViewDetailsService viewDetailsService;

    @RequestMapping(value = "home/createPackageDetails")
    public ViewDetails createPackageDetails(@RequestBody ViewDetailsDTO viewDetailsDTO){
        return viewDetailsService.createPackageDetails(viewDetailsDTO);
    }

    @RequestMapping(value = "/home/getViewDetails",method = RequestMethod.GET)
    public List<ViewDetails> gatViewDetails(@RequestParam(value="city") String city){

        List<ViewDetails> viewDetails = viewDetailsService.getViewDetails(city);
        return viewDetails;
    }
}
