package com.yogi.traveltriangle.controllers;

import com.yogi.traveltriangle.DTO.HotelsDTO;
import com.yogi.traveltriangle.models.Hotels;
import com.yogi.traveltriangle.services.HotelsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
public class HotelsController {

    @Autowired
    private HotelsService hotelsService;

    @RequestMapping(value = "home/createHotels")
    public Hotels createHotelsDetails(@RequestBody HotelsDTO hotelsDTO){
        return hotelsService.createHotels(hotelsDTO);
    }

    @RequestMapping(value = "/home/getHotelsDetails",method = RequestMethod.GET)
    public List<Hotels> gatHotelsDetails(@RequestParam(value="city") String city){

        List<Hotels> hotelsDetails = hotelsService.getHotelsDetails(city);
        return hotelsDetails;
    }
}
