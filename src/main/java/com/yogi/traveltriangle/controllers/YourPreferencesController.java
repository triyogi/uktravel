package com.yogi.traveltriangle.controllers;

import com.yogi.traveltriangle.DTO.YourPreferencesDTO;
import com.yogi.traveltriangle.models.YourPreferences;
import com.yogi.traveltriangle.services.YourPreferencesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class YourPreferencesController {
    @Autowired
    private YourPreferencesService yourPreferencesService;

    @RequestMapping(value = "home/createYourPreferences")
    public YourPreferences createYourPreferences(@RequestBody YourPreferencesDTO yourPreferencesDTO){
        return yourPreferencesService.createYourPreferences(yourPreferencesDTO);
    }

    @RequestMapping(value = "/home/getYourPreferences",method = RequestMethod.GET)
    public List<YourPreferences> gatViewDetails(@RequestParam(value="arrivalDestination") String arrivalDestination){

        List<YourPreferences> yourPreferences = yourPreferencesService.getYourPreferences(arrivalDestination);
        return yourPreferences;
    }
}
