package com.yogi.traveltriangle.controllers;

import com.yogi.traveltriangle.DTO.PackagesDTO;
import com.yogi.traveltriangle.models.Packages;
import com.yogi.traveltriangle.services.PackagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
public class PackagesController {

    @Autowired
    private PackagesService packagesService;

    @RequestMapping(value = "/home/createpackages",method = RequestMethod.POST)
    public Packages createPackages(@RequestBody PackagesDTO packagesDTO){
       return packagesService.createPackages(packagesDTO);
    }

    @RequestMapping(value = "/home/getallpackages",method = RequestMethod.GET)
    public List<Packages> gatAllPackages(@RequestParam(value="city", required = false) String city,
                                         @RequestParam(value="ratingOfPackage", required = false) String ratingOfPackage) {

        List<Packages> myPackages = null;
        if (city!=null && ratingOfPackage!=null) {
            myPackages = packagesService.getPackages(city, ratingOfPackage);
        }
        else if(city!=null & ratingOfPackage==null)
        {
            myPackages = packagesService.getPackagesByCity(city);
        }
        else if(ratingOfPackage!=null & city==null){
            myPackages = packagesService.getPackagesByRating(ratingOfPackage);
        }
        //List<Packages> myPackages = packagesService.getPackages(city,ratingOfPackage);
        return myPackages;
    }
}
