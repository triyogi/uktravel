package com.yogi.traveltriangle.config;

import com.yogi.traveltriangle.models.User;
import org.hibernate.Session;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
@ComponentScan(basePackages = "com.yogi.traveltriangle")
public class LocalDBServiceImp {

    public  void LocalDBServiceImp() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User newUser = new User();
        newUser.setFullName("Yogita Tripathi");

        session.saveOrUpdate(newUser);

        session.getTransaction().commit();
        session.close();

        HibernateUtil.shutdown();
    }
}
