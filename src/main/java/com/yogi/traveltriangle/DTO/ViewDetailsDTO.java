package com.yogi.traveltriangle.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ViewDetailsDTO {

    private Long id;
    private String title;
    private String packageDuration;
    private String ratingOfIncludedHotel;
    private String city;

    @JsonProperty
    private boolean isFlightIncluded;

    @JsonProperty
    private boolean isBreakfastIncluded;

    @JsonProperty
    private boolean isSightseeingIncluded;

    @JsonProperty
    private boolean isTransferable;

    private String month;
    private String price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPackageDuration() {
        return packageDuration;
    }

    public void setPackageDuration(String packageDuration) {
        this.packageDuration = packageDuration;
    }

    public String getRatingOfIncludedHotel() {
        return ratingOfIncludedHotel;
    }

    public void setRatingOfIncludedHotel(String ratingOfIncludedHotel) {
        this.ratingOfIncludedHotel = ratingOfIncludedHotel;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isFlightIncluded() {
        return isFlightIncluded;
    }

    public void setFlightIncluded(boolean flightIncluded) {
        isFlightIncluded = flightIncluded;
    }

    public boolean isBreakfastIncluded() {
        return isBreakfastIncluded;
    }

    public void setBreakfastIncluded(boolean breakfastIncluded) {
        isBreakfastIncluded = breakfastIncluded;
    }

    public boolean isSightseeingIncluded() {
        return isSightseeingIncluded;
    }

    public void setSightseeingIncluded(boolean sightseeingIncluded) {
        isSightseeingIncluded = sightseeingIncluded;
    }

    public boolean isTransferable() {
        return isTransferable;
    }

    public void setTransferable(boolean transferable) {
        isTransferable = transferable;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
