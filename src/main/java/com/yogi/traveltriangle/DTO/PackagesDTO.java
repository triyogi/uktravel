package com.yogi.traveltriangle.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class PackagesDTO {

    private Long id;

    private String titleOfPackage;
    private BigDecimal priceOfPackage;
    private String starOfHotel;

    @JsonProperty
    private boolean isFlightIncluded;

    @JsonProperty
    private boolean isMealIncluded;

    @JsonProperty
    private boolean isTransferable;
    private String city;
    private String ratingOfPackage;

    public String getRatingOfPackage() {
        return ratingOfPackage;
    }

    public void setRatingOfPackage(String ratingOfPackage) {
        this.ratingOfPackage = ratingOfPackage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitleOfPackage() {
        return titleOfPackage;
    }

    public void setTitleOfPackage(String titleOfPackage) {
        this.titleOfPackage = titleOfPackage;
    }

    public BigDecimal getPriceOfPackage() {
        return priceOfPackage;
    }

    public void setPriceOfPackage(BigDecimal priceOfPackage) {
        this.priceOfPackage = priceOfPackage;
    }

    public String getStarOfHotel() {
        return starOfHotel;
    }

    public void setStarOfHotel(String starOfHotel) {
        this.starOfHotel = starOfHotel;
    }

    public boolean getIsFlightIncluded() {
        return isFlightIncluded;
    }



    public void setIsFlightIncluded(boolean flightIncluded) {
        isFlightIncluded = flightIncluded;
    }

    public boolean isMealIncluded() {
        return isMealIncluded;
    }

    public void setMealIncluded(boolean mealIncluded) {
        isMealIncluded = mealIncluded;
    }

    public boolean isTransferable() {
        return isTransferable;
    }

    public void setTransferable(boolean transferable) {
        isTransferable = transferable;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
