package com.yogi.traveltriangle.DTO;

public class HotelsDTO {
    private Long id;
    private String days;
    private String hotelCity;
    private String hotelName;
    private String ratingOfHotel;
    private String hotelAddress;
    private String reviewCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getHotelCity() {
        return hotelCity;
    }

    public void setHotelCity(String hotelCity) {
        this.hotelCity = hotelCity;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getRatingOfHotel() {
        return ratingOfHotel;
    }

    public void setRatingOfHotel(String ratingOfHotel) {
        this.ratingOfHotel = ratingOfHotel;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }
}
