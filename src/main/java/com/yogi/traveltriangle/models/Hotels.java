package com.yogi.traveltriangle.models;

import javax.persistence.*;

@Entity
public class Hotels {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "Days")
    private String days;

    @Column(name = "HotelCity")
    private String hotelCity;

    @Column(name = "HotelName")
    private String hotelName;

    @Column(name = "RatingOfHotel")
    private String ratingOfHotel;

    @Column(name = "HotelAddress")
    private String hotelAddress;

    @Column(name = "ReviewCount")
    private String reviewCount;

    @OneToOne
    private ViewDetails viewDetails;

    public ViewDetails getViewDetails() {
        return viewDetails;
    }

    public void setViewDetails(ViewDetails viewDetails) {
        this.viewDetails = viewDetails;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getHotelCity() {
        return hotelCity;
    }

    public void setHotelCity(String hotelCity) {
        this.hotelCity = hotelCity;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getRatingOfHotel() {
        return ratingOfHotel;
    }

    public void setRatingOfHotel(String ratingOfHotel) {
        this.ratingOfHotel = ratingOfHotel;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }
}
