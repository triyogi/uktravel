package com.yogi.traveltriangle.models;

import javax.persistence.*;

@Entity
public class Filters {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "Categories")
    private String categories;

    @Column(name = "TypeOfDestination")
    private String typeOfDestination;

    @Column(name = "DurationInDays")
    private String durationInDays;

    @Column(name = "BudgetPerPersonInRs")
    private String budgetPerPersonInRs;

    @Column(name = "HotelStarRating")
    private String hotelStarRating;

    @Column(name = "Inclusions")
    private String inclusions;

    @Column(name = "Activities")
    private String activities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getTypeOfDestination() {
        return typeOfDestination;
    }

    public void setTypeOfDestination(String typeOfDestination) {
        this.typeOfDestination = typeOfDestination;
    }

    public String getDurationInDays() {
        return durationInDays;
    }

    public void setDurationInDays(String durationInDays) {
        this.durationInDays = durationInDays;
    }

    public String getBudgetPerPersonInRs() {
        return budgetPerPersonInRs;
    }

    public void setBudgetPerPersonInRs(String budgetPerPersonInRs) {
        this.budgetPerPersonInRs = budgetPerPersonInRs;
    }

    public String getHotelStarRating() {
        return hotelStarRating;
    }

    public void setHotelStarRating(String hotelStarRating) {
        this.hotelStarRating = hotelStarRating;
    }

    public String getInclusions() {
        return inclusions;
    }

    public void setInclusions(String inclusions) {
        this.inclusions = inclusions;
    }

    public String getActivities() {
        return activities;
    }

    public void setActivities(String activities) {
        this.activities = activities;
    }
}
