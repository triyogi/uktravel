package com.yogi.traveltriangle.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "packages")
public class Packages {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "titleOfPackage")
    private String titleOfPackage;

    @Column(name = "priceOfPackage")
    private BigDecimal priceOfPackage;

    @Column(name = "starOfHotel")
    private String starOfHotel;

    @Column(name = "isFlightIncluded")
    private boolean isFlightIncluded;

    @Column(name = "isMealIncluded")
    private boolean isMealIncluded;

    @Column(name = "isTransferable")
    private boolean isTransferable;
    //private List<Attraction> attractionDetails;

    @Column(name = "city")
    private String city;

    @Column(name = "ratingOfPackage")
    private String ratingOfPackage;

    @Override
    public String toString() {
        return "Packages{" +
                "id=" + id +
                ", titleOfPackage='" + titleOfPackage + '\'' +
                ", priceOfPackage=" + priceOfPackage +
                ", starOfHotel='" + starOfHotel + '\'' +
                ", isFlightIncluded=" + isFlightIncluded +
                ", isMealIncluded=" + isMealIncluded +
                ", isTransferable=" + isTransferable +
                ", city='" + city + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitleOfPackage() {
        return titleOfPackage;
    }

    public void setTitleOfPackage(String titleOfPackage) {
        this.titleOfPackage = titleOfPackage;
    }

    public BigDecimal getPriceOfPackage() {
        return priceOfPackage;
    }

    public void setPriceOfPackage(BigDecimal priceOfPackage) {
        this.priceOfPackage = priceOfPackage;
    }

    public String getStarOfHotel() {
        return starOfHotel;
    }

    public void setStarOfHotel(String starOfHotel) {
        this.starOfHotel = starOfHotel;
    }

    public boolean isFlightIncluded() {
        return isFlightIncluded;
    }

    public void setFlightIncluded(boolean flightIncluded) {
        isFlightIncluded = flightIncluded;
    }

    public boolean isMealIncluded() {
        return isMealIncluded;
    }

    public void setMealIncluded(boolean mealIncluded) {
        isMealIncluded = mealIncluded;
    }

    public boolean isTransferable() {
        return isTransferable;
    }

    public void setTransferable(boolean transferable) {
        isTransferable = transferable;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public String getRatingOfPackage() {
        return ratingOfPackage;
    }

    public void setRatingOfPackage(String ratingOfPackage) {
        this.ratingOfPackage = ratingOfPackage;
    }
}