package com.yogi.traveltriangle.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
public class YourPreferences {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ArrivalDestination")
    private String arrivalDestination;

    @Column(name = "DepartureDestination")
    private String departureDestination;

    @JsonProperty
    @Column(name = "IsCheckedExploringDestinations")
    private boolean isCheckedExploringDestinations;

    @Column(name = "DepartureDate")
    private String departureDate;

    @Column(name = "EmailId")
    private String emailId;

    @Column(name = "PhoneNumber")
    private String phoneNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArrivalDestination() {
        return arrivalDestination;
    }

    public void setArrivalDestination(String arrivalDestination) {
        this.arrivalDestination = arrivalDestination;
    }

    public String getDepartureDestination() {
        return departureDestination;
    }

    public void setDepartureDestination(String departureDestination) {
        this.departureDestination = departureDestination;
    }

    public boolean isCheckedExploringDestinations() {
        return isCheckedExploringDestinations;
    }

    public void setCheckedExploringDestinations(boolean checkedExploringDestinations) {
        isCheckedExploringDestinations = checkedExploringDestinations;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
