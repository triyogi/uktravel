package com.yogi.traveltriangle.models;

import javax.persistence.*;

@Entity
public class ViewDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "Title")
    private String title;

    @Column(name = "PackageDuration")
    private String packageDuration;

    @Column(name = "RatingOfIncludedHotel")
    private String ratingOfIncludedHotel;

    @Column(name = "City")
    private String city;

    @Column(name = "IsFlightIncluded")
    private boolean isFlightIncluded;

    @Column(name = "IsBreakfastIncluded")
    private boolean isBreakfastIncluded;

    @Column(name = "IsSightseeingIncluded")
    private boolean isSightseeingIncluded;

    @Column(name = "IsTransferable")
    private boolean isTransferable;

    @Column(name = "Month")
    private String month;

    @Column(name = "Price")
    private String price;

    @OneToOne(cascade = CascadeType.ALL)
    private Hotels hotels;

    public Hotels getHotels() {
        return hotels;
    }

    public void setHotels(Hotels hotels) {
        this.hotels = hotels;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPackageDuration() {
        return packageDuration;
    }

    public void setPackageDuration(String packageDuration) {
        this.packageDuration = packageDuration;
    }

    public String getRatingOfIncludedHotel() {
        return ratingOfIncludedHotel;
    }

    public void setRatingOfIncludedHotel(String ratingOfIncludedHotel) {
        this.ratingOfIncludedHotel = ratingOfIncludedHotel;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isFlightIncluded() {
        return isFlightIncluded;
    }

    public void setFlightIncluded(boolean flightIncluded) {
        isFlightIncluded = flightIncluded;
    }

    public boolean isBreakfastIncluded() {
        return isBreakfastIncluded;
    }

    public void setBreakfastIncluded(boolean breakfastIncluded) {
        isBreakfastIncluded = breakfastIncluded;
    }

    public boolean isSightseeingIncluded() {
        return isSightseeingIncluded;
    }

    public void setSightseeingIncluded(boolean sightseeingIncluded) {
        isSightseeingIncluded = sightseeingIncluded;
    }

    public boolean isTransferable() {
        return isTransferable;
    }

    public void setTransferable(boolean transferable) {
        isTransferable = transferable;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
