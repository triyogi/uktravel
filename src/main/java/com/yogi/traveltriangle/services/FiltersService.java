package com.yogi.traveltriangle.services;

import com.yogi.traveltriangle.DTO.FiltersDTO;
import com.yogi.traveltriangle.models.Filters;
import com.yogi.traveltriangle.repositories.FiltersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FiltersService {

    @Autowired
    private FiltersRepository filtersRepository;

    public Filters createFilters(FiltersDTO filtersDTO) {
        return filtersRepository.createFilters(filtersDTO);
    }

    public List<Filters> getAllFilters(String hotelStarRating) {
        return filtersRepository.getAllFilters(hotelStarRating);
    }
}
