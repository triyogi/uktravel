package com.yogi.traveltriangle.services;

import com.yogi.traveltriangle.DTO.UserDTO;
import com.yogi.traveltriangle.models.User;
import com.yogi.traveltriangle.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User createUser(UserDTO userDTO){
        return userRepository.saveInDB(userDTO);
    }
}
