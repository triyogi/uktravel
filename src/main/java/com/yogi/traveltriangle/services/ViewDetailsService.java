package com.yogi.traveltriangle.services;

import com.yogi.traveltriangle.DTO.ViewDetailsDTO;
import com.yogi.traveltriangle.models.ViewDetails;
import com.yogi.traveltriangle.repositories.ViewDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ViewDetailsService {

    @Autowired
    private ViewDetailsRepository viewDetailsRepository;

    public ViewDetails createPackageDetails(ViewDetailsDTO viewDetailsDTO) {
        return viewDetailsRepository.createPackageDetails(viewDetailsDTO);
    }

    public List<ViewDetails> getViewDetails(String city) {
        return viewDetailsRepository.getViewDetails(city);
    }
}
