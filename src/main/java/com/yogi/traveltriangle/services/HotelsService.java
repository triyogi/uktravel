package com.yogi.traveltriangle.services;

import com.yogi.traveltriangle.DTO.HotelsDTO;
import com.yogi.traveltriangle.models.Hotels;
import com.yogi.traveltriangle.repositories.HotelsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelsService {

    @Autowired
    private HotelsRepository hotelsRepository;

    public Hotels createHotels(HotelsDTO hotelsDTO) {
        return hotelsRepository.createHotels(hotelsDTO);
    }

    public List<Hotels> getHotelsDetails(String city) {
        return hotelsRepository.getHotelsDetails(city);
    }
}
