package com.yogi.traveltriangle.services;

import com.yogi.traveltriangle.DTO.PackagesDTO;
import com.yogi.traveltriangle.models.Packages;
import com.yogi.traveltriangle.repositories.PackagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PackagesService {

    @Autowired
    private PackagesRepository packagesRepository;

    public List<Packages> getPackages(String city,String ratingOfPackage){
        return  packagesRepository.getAllPackage(city,ratingOfPackage);
    }

    public List<Packages> getPackagesByRating(String ratingOfPackage){
        return  packagesRepository.getPackagesByRating(ratingOfPackage);
    }

    public List<Packages> getPackagesByCity(String city){
        return  packagesRepository.getPackagesByCity(city);
    }

    public Packages createPackages(PackagesDTO packagesDTO){
        return packagesRepository.createPackages(packagesDTO);
    }
}
