package com.yogi.traveltriangle.services;

import com.yogi.traveltriangle.DTO.YourPreferencesDTO;
import com.yogi.traveltriangle.models.YourPreferences;
import com.yogi.traveltriangle.repositories.YourPreferencesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YourPreferencesService {

    @Autowired
    private YourPreferencesRepository yourPreferencesRepository;

    public YourPreferences createYourPreferences(YourPreferencesDTO yourPreferencesDTO) {
        return yourPreferencesRepository.createYourPreferences(yourPreferencesDTO);
    }

    public List<YourPreferences> getYourPreferences(String arrivalDestination) {
        return yourPreferencesRepository.getYourPreferences(arrivalDestination);
    }
}
