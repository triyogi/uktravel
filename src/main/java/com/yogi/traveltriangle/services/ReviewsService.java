package com.yogi.traveltriangle.services;

import com.yogi.traveltriangle.DTO.ReviewsDTO;
import com.yogi.traveltriangle.models.Reviews;
import com.yogi.traveltriangle.repositories.ReviewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewsService {

    @Autowired
    private ReviewsRepository reviewsRepository;

    public Reviews createReviews(ReviewsDTO reviewsDTO){
        return reviewsRepository.createReviews(reviewsDTO);
    }

    public List<Reviews> getAllReviews(String reviewtitle){
        return reviewsRepository.getAllReviews(reviewtitle);
    }
}
