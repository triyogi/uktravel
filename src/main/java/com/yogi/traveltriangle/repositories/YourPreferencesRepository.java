package com.yogi.traveltriangle.repositories;

import com.yogi.traveltriangle.DTO.YourPreferencesDTO;
import com.yogi.traveltriangle.config.HibernateUtil;
import com.yogi.traveltriangle.models.YourPreferences;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class YourPreferencesRepository {
    public YourPreferences createYourPreferences(YourPreferencesDTO yourPreferencesDTO) {
        Session sessionFactory= HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        YourPreferences yourPreferences=new YourPreferences();
        yourPreferences.setArrivalDestination(yourPreferencesDTO.getArrivalDestination());
        yourPreferences.setDepartureDestination(yourPreferencesDTO.getDepartureDestination());
        yourPreferences.setCheckedExploringDestinations(yourPreferencesDTO.isCheckedExploringDestinations());
        yourPreferences.setDepartureDate(yourPreferencesDTO.getDepartureDate());
        yourPreferences.setEmailId(yourPreferencesDTO.getEmailId());
        yourPreferences.setPhoneNumber(yourPreferencesDTO.getPhoneNumber());

        sessionFactory.save(yourPreferences);
        sessionFactory.getTransaction().commit();
        sessionFactory.close();

        return yourPreferences;
    }

    public List<YourPreferences> getYourPreferences(String arrivalDestination) {
        Session sessionFactory= HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        String hql = "from YourPreferences where ArrivalDestination like :arrivalDestination";

        Query query = sessionFactory.createQuery(hql);
        query.setParameter("arrivalDestination", "%" + arrivalDestination + "%");

        List<YourPreferences> allYourPreferences=query.getResultList();
        sessionFactory.getTransaction().commit();
        sessionFactory.close();

        return allYourPreferences;
    }
}
