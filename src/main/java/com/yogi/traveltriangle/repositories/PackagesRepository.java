package com.yogi.traveltriangle.repositories;

import com.yogi.traveltriangle.DTO.PackagesDTO;
import com.yogi.traveltriangle.config.HibernateUtil;
import com.yogi.traveltriangle.models.Packages;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class PackagesRepository {

    public List<Packages> getAllPackage(String city, String ratingOfPackage){

        Session sessionFactory = HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        String hql = "from Packages where city like :city and ratingOfPackage like :ratingOfPackage";

        Query query = sessionFactory.createQuery(hql);
        query.setParameter("city", "%" + city + "%");
        query.setParameter("ratingOfPackage", "%" + ratingOfPackage + "%");

        List<Packages> myPackages  = query.getResultList();

        sessionFactory.getTransaction().commit();
        sessionFactory.close();
        return myPackages;
    }

    public List<Packages> getPackagesByRating(String ratingOfPackage) {

        Session sessionFactory = HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        String hql = "from Packages where ratingOfPackage like :ratingOfPackage";

        Query query = sessionFactory.createQuery(hql);
        query.setParameter("ratingOfPackage", "%" + ratingOfPackage + "%");

                List<Packages> myPackages = query.getResultList();

                sessionFactory.getTransaction().commit();
                sessionFactory.close();
                return myPackages;        
    }


    public Packages createPackages(PackagesDTO packagesDTO){
        Session sessionFactory = HibernateUtil.getSessionFactory().openSession();
        Packages packages = new Packages();
        packages.setTitleOfPackage(packagesDTO.getTitleOfPackage());
        packages.setPriceOfPackage(packagesDTO.getPriceOfPackage());
        packages.setStarOfHotel(packagesDTO.getStarOfHotel());
        packages.setFlightIncluded(packagesDTO.getIsFlightIncluded());
        packages.setMealIncluded(packagesDTO.isMealIncluded());
        packages.setTransferable(packagesDTO.isTransferable());
        packages.setCity(packagesDTO.getCity());
        packages.setRatingOfPackage(packagesDTO.getRatingOfPackage());

        sessionFactory.beginTransaction();
        sessionFactory.save(packages);

        sessionFactory.getTransaction().commit();
        sessionFactory.close();
        return packages;
    }

    public List<Packages> getPackagesByCity(String city) {
        Session sessionFactory = HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        String hql = "from Packages where city like :city";

        Query query = sessionFactory.createQuery(hql);
        query.setParameter("city", "%" + city + "%");

        List<Packages> myPackages = query.getResultList();

        sessionFactory.getTransaction().commit();
        sessionFactory.close();
        return myPackages;
    }
}
