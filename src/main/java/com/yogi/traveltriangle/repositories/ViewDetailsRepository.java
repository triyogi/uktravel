package com.yogi.traveltriangle.repositories;

import com.yogi.traveltriangle.DTO.ViewDetailsDTO;
import com.yogi.traveltriangle.config.HibernateUtil;
import com.yogi.traveltriangle.models.Packages;
import com.yogi.traveltriangle.models.ViewDetails;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ViewDetailsRepository {

    public ViewDetails createPackageDetails(ViewDetailsDTO viewDetailsDTO) {
        Session sessionFactory= HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        ViewDetails viewDetails=new ViewDetails();

        viewDetails.setTitle(viewDetailsDTO.getTitle());
        viewDetails.setPackageDuration(viewDetailsDTO.getPackageDuration());
        viewDetails.setRatingOfIncludedHotel(viewDetailsDTO.getRatingOfIncludedHotel());
        viewDetails.setCity(viewDetailsDTO.getCity());
        viewDetails.setFlightIncluded(viewDetailsDTO.isFlightIncluded());
        viewDetails.setBreakfastIncluded(viewDetailsDTO.isBreakfastIncluded());
        viewDetails.setSightseeingIncluded(viewDetailsDTO.isSightseeingIncluded());
        viewDetails.setTransferable(viewDetailsDTO.isTransferable());
        viewDetails.setMonth(viewDetailsDTO.getMonth());
        viewDetails.setPrice(viewDetailsDTO.getPrice());

        sessionFactory.save(viewDetails);
        sessionFactory.getTransaction().commit();
        sessionFactory.close();
        return viewDetails;
    }

    public List<ViewDetails> getViewDetails(String city) {
        Session sessionFactory = HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        String hql = "from ViewDetails where City like :city";

        Query query = sessionFactory.createQuery(hql);
        query.setParameter("city", "%" + city + "%");

        //  Query query = sessionFactory.createQuery("from Packages where city= :"+city).setParameter("city",city);

        List<ViewDetails> myViewDetails  = query.getResultList();

        // List<Packages> myPackages = sessionFactory.createQuery("from Packages where city= :"+city).setParameter("city",city).list();
        sessionFactory.getTransaction().commit();
        sessionFactory.close();
        return myViewDetails;
    }
}
