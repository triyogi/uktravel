package com.yogi.traveltriangle.repositories;

import com.yogi.traveltriangle.DTO.FiltersDTO;
import com.yogi.traveltriangle.config.HibernateUtil;
import com.yogi.traveltriangle.models.Filters;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class FiltersRepository {

    public Filters createFilters(FiltersDTO filtersDTO) {
        Session sessionFectory= HibernateUtil.getSessionFactory().openSession();
        sessionFectory.beginTransaction();

        Filters filters=new Filters();
        filters.setTypeOfDestination(filtersDTO.getTypeOfDestination());
        filters.setCategories(filtersDTO.getCategories());
        filters.setDurationInDays(filtersDTO.getDurationInDays());
        filters.setBudgetPerPersonInRs(filtersDTO.getBudgetPerPersonInRs());
        filters.setHotelStarRating(filtersDTO.getHotelStarRating());
        filters.setInclusions(filtersDTO.getInclusions());
        filters.setActivities(filtersDTO.getActivities());

        sessionFectory.save(filters);
        sessionFectory.getTransaction().commit();
        sessionFectory.close();

        return filters;
    }

    public List<Filters> getAllFilters(String hotelStarRating) {
        Session sessionFectory= HibernateUtil.getSessionFactory().openSession();
        sessionFectory.beginTransaction();

        String hql = "from Filters where HotelStarRating like :HotelStarRating";

        Query query = sessionFectory.createQuery(hql);
        query.setParameter("HotelStarRating", "%" + hotelStarRating + "%");

        List<Filters> filters=query.getResultList();
        sessionFectory.getTransaction().commit();
        sessionFectory.close();
        return filters;
    }
}
