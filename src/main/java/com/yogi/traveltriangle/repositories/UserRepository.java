package com.yogi.traveltriangle.repositories;

import com.yogi.traveltriangle.DTO.UserDTO;
import com.yogi.traveltriangle.config.HibernateUtil;
import com.yogi.traveltriangle.models.User;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class UserRepository {

    public User saveInDB(UserDTO userDTO) {
        Session sessionFactory = HibernateUtil.getSessionFactory().openSession();
        User user = new User();
        user.setFullName(userDTO.getFullName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setConfirmPassword(userDTO.getConfirmPassword());
        user.setPhoneNumber(userDTO.getPhoneNumber());

        sessionFactory.beginTransaction();
        sessionFactory.save(user);

        sessionFactory.getTransaction().commit();
        sessionFactory.close();
        return user;
    }
}
