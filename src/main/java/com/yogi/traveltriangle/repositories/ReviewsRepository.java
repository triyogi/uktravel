package com.yogi.traveltriangle.repositories;

import com.yogi.traveltriangle.DTO.ReviewsDTO;
import com.yogi.traveltriangle.config.HibernateUtil;
import com.yogi.traveltriangle.models.Reviews;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ReviewsRepository {

    public Reviews createReviews(ReviewsDTO reviewsDTO){
        Session sessionFactory= HibernateUtil.getSessionFactory().openSession();
        Reviews reviews=new Reviews();

        reviews.setReviewTitle(reviewsDTO.getReviewTitle());
        reviews.setRatingOfReview(reviewsDTO.getRatingOfReview());
        reviews.setUserName(reviewsDTO.getUserName());
        reviews.setUserLocation(reviewsDTO.getUserLocation());
        reviews.setPostDate(reviewsDTO.getPostDate());
        reviews.setCommentDescription(reviewsDTO.getCommentDescription());

        sessionFactory.beginTransaction();
        sessionFactory.save(reviews);

        sessionFactory.getTransaction().commit();
        sessionFactory.close();

        return reviews;
    }

    public List<Reviews> getAllReviews(String reviewtitle){
        Session sessionFactory=HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        String hql = "from Reviews where reviewTitle like :reviewTitle";

        Query query = sessionFactory.createQuery(hql);
        query.setParameter("reviewTitle", "%" + reviewtitle + "%");

        List<Reviews> allReviews=query.getResultList();
        sessionFactory.getTransaction().commit();
        sessionFactory.close();

        return allReviews;
    }
}
