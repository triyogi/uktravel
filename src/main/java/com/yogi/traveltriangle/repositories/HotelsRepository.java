package com.yogi.traveltriangle.repositories;

import com.yogi.traveltriangle.DTO.HotelsDTO;
import com.yogi.traveltriangle.config.HibernateUtil;
import com.yogi.traveltriangle.models.Hotels;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class HotelsRepository {
    public Hotels createHotels(HotelsDTO hotelsDTO) {
        Session sessionFactory= HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        Hotels hotels=new Hotels();
        hotels.setDays(hotelsDTO.getDays());
        hotels.setHotelCity(hotelsDTO.getHotelCity());
        hotels.setHotelName(hotelsDTO.getHotelName());
        hotels.setRatingOfHotel(hotelsDTO.getRatingOfHotel());
        hotels.setHotelAddress(hotelsDTO.getHotelAddress());
        hotels.setReviewCount(hotelsDTO.getReviewCount());

        sessionFactory.save(hotels);
        sessionFactory.getTransaction().commit();
        sessionFactory.close();
         return hotels;
    }

    public List<Hotels> getHotelsDetails(String city) {
        Session sessionFactory=HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();

        String hql = "from Hotels where HotelCity like :city";

        Query query = sessionFactory.createQuery(hql);
        query.setParameter("city", "%" + city + "%");

        List<Hotels> allHotelsDetails=query.getResultList();
        sessionFactory.getTransaction().commit();
        sessionFactory.close();

        return allHotelsDetails;
    }
}
